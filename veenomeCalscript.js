   //var head = document.getElementsByTagName('head')[0];
   var body=$("body");
	//	if($("body").attr("class"))
	//	{
	//	 body = $("#onegoogbar").next();
	//
	//	}

	$("body").prepend('<style>#LoadingDiv{	margin:0px 0px 0px 0px;	position:fixed;	height: 100%;	z-index:9999;	padding-top:400px;	padding-left:650px;	width:100%;	clear:none; opacity:0.4;	background:url("'+chrome.extension.getURL("transbg.png")+'");	border:1px solid #000000;	}</style><div id="LoadingDiv" style="display:none;"><img src="'+chrome.extension.getURL("ajax-loader.gif")+'" alt="" /></div>');

    var extGTMScript = {
		'ldiv':'',
		'class2':'',
		'selectedGoogleEvent':''
	   }; 
	
	function createGTMeeting_extnGTMS(event)
	{ 
		extGTMScript.ldiv = document.getElementById('LoadingDiv');         
		if(event.target.checked)
		{
			extGTMScript.ldiv.style.display='block';

			extGTMScript.selectedGoogleEvent= {
			'eventTitle':  $('.ui-sch input').val(),
			'sMonthYr':    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:first').val(),
			'sTime'   :    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:first').next().val(),
			'eMonthYr':    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:last').val(),
			'eTime'   :    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:last').prev().val()
			};
			
			chrome.extension.sendRequest({'selectedGoogleEvent' : extGTMScript.selectedGoogleEvent,'action': 'createGTMeeting'});
		}
		else
		{
			extGTMScript.ldiv.style.display='none';
		}
	}

	function getGTMDescriptionText(meetingId, conferenceCallInfo) {
		return "== GoTo Meeting ==\nhttps://www4.gotomeeting.com/join/" + meetingId + "\n" + conferenceCallInfo;
	}

	function getLocationInput() {
		return document.getElementsByClassName("ui-sch")[2].getElementsByTagName("input")[0];
	}

	function hasLocation() {
		return getLocationInput().value.length != 0;
	}

	function appendLocation(location) {
		var locationInput = getLocationInput();
		locationInput.focus();
		document.execCommand('inserthtml', false, location);
	}

	function getDescriptionInput() {
		return document.getElementsByClassName("ep-dp-descript")[0].getElementsByClassName("textinput")[0];
	}

	function hasDescription() {
		return getDescriptionInput().value.length != 0;
	}

	function appendDescription(description) {
		getDescriptionInput().focus();
		document.execCommand('inserthtml', false, description);	
	}

	function extInitiate_extnGTMS(event)
	{ 
		if(top.location.href.indexOf("https://www.google.com/calendar/")!=-1)
		{
		  var guestListDiv = document.getElementsByClassName("ep-go"); 
		  if(guestListDiv.length)
			{ 
			
			  if(!document.getElementById("gtmDiv"))
				{				 
				  
					var gtmDiv = document.createElement("div");
					gtmDiv.setAttribute('id','gtmDiv');
					
					var gtmCheck = "<div style='padding-top:20px;'><input type='checkbox' id='inviteChk'/>Add GoToMeeting Invite.</div>";
					guestListDiv[0].parentNode.appendChild(gtmDiv);
					var gtmUpdateButton = "<div id='gtmUpdateButton' style='padding-top:5px;background:url("+chrome.extension.getURL('plus-256.png')+") no-repeat;cursor: pointer;'><div style='margin-left:20px;margin-top:-3px;'>Update GoToMeeting with New Date & Time</div></div>";

					gtmDiv.innerHTML =gtmCheck+gtmUpdateButton;
					    chrome.extension.sendRequest({
						'action': 'showPageAction'
					});
					document.getElementById('inviteChk').addEventListener('click',createGTMeeting_extnGTMS); 	
										document.getElementById('gtmUpdateButton').addEventListener('click',function(){
													extGTMScript.ldiv = document.getElementById('LoadingDiv');   
													extGTMScript.ldiv.style.display='block';


											extGTMScript.selectedGoogleEvent= {
			'eventTitle':  $('.ui-sch input').val(),
			'sMonthYr':    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:first').val(),
			'sTime'   :    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:first').next().val(),
			'eMonthYr':    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:last').val(),
			'eTime'   :    $('#coverinner div.ep div.ep-dpc div.ui-sch div.drs span.ep-edr-first-line span span span input:last').prev().val()
			};
			var class1 = document.getElementsByClassName("ep-dp-descript"); 		
			var currEventDesc = class1[0].getElementsByClassName("textinput")[0].value; 
			var searchText ="https://www4.gotomeeting.com/join/"; 
			var meetingGTMId = currEventDesc.substring(currEventDesc.indexOf(searchText)+searchText.length,currEventDesc.indexOf(searchText)+searchText.length+9);	if(meetingGTMId.length)
				chrome.extension.sendRequest({'getGoogleEvent' : meetingGTMId,'selectedGoogleEvent' : extGTMScript.selectedGoogleEvent,'action': 'getMeetings_extnGTM'});
			else
				{
				    extGTMScript.ldiv.style.display='none';
					alert('Meeting not added in GTM.'); 
					return;

				}

			    });
			}
		}
	}

	}

	document.addEventListener('DOMSubtreeModified',extInitiate_extnGTMS);
	document.addEventListener('hashchange',extInitiate_extnGTMS);
	document.addEventListener('load',extInitiate_extnGTMS);

	chrome.extension.onMessage.addListener(
	  function(request, sender, sendResponse) { 
	   if(request.action=='populateDesc')
		{  			
	        var extnGTMre = request.result_desc;
			var meetingId = extnGTMre[0].meetingId.toString();
			var conferenceCallInfo = extnGTMre[0].conferenceCallInfo;
		    var extnGTMiso=extnGTMre[0].startTime;
		    extnGTMiso =  extnGTMiso.substring(0,extnGTMiso.length-6)+"Z";

			var utc = new Date(extnGTMiso);
			var local = new Date(utc.getTime() + utc.getTimezoneOffset()); 
		    var meetingIdn=meetingId.substring(0, 3)+"-"+meetingId.substring(3, 6)+"-"+meetingId.substring(6, 9);

			// turn off loading div
			extGTMScript.ldiv.style.display = 'none';

			var description = getGTMDescriptionText(meetingId, conferenceCallInfo);
			if(hasDescription()) {
				description = '\n\n'+description;
			}
			appendDescription(description);

			// update the location if needed
			if(!hasLocation()) {
				appendLocation('GoTo Meeting');
			}

			// pop focus back and forth so google expands the meeting description
			getLocationInput().focus();
			getDescriptionInput().focus();
     	}
		if(request.action=='updateDesc')
		{ 
			extGTMScript.ldiv.style.display='none';

			 alert('Meeting Updated.');
/*

// RC 9/5/13: don't worry about updating the text of the meeting since it'd still be the same
             var extnGTMiso=request.update_descText;
		 //   extnGTMiso =  extnGTMiso.substring(0,extnGTMiso.length-6)+"Z";

			var utc = new Date(extnGTMiso);
			var local = new Date(utc.getTime() + utc.getTimezoneOffset()); 


			var class1 = document.getElementsByClassName("ep-dp-descript"); 		
			extGTMScript.class2 = class1[0].getElementsByClassName("textinput")[0]; 
			var searchTxt1="Please join my meeting,";
			var searchTxt2="https://www4.gotomeeting.com/join/";
			 var currEventDesc = class1[0].getElementsByClassName("textinput")[0].value; 

			 searchTxt1Indx =currEventDesc.indexOf(searchTxt1); 
			 searchTxt2Indx=currEventDesc.indexOf(searchTxt2);
			 var updatedDesc = currEventDesc.substring(0,searchTxt1Indx+searchTxt1.length)+local+currEventDesc.substring(searchTxt2Indx,currEventDesc.length);
			 extGTMScript.class2.value = '';

			extGTMScript.class2.focus();

			document.execCommand('inserthtml', false, updatedDesc);
*/
			
		}
  });

