  
 initScripts_extnGTM();

 var syncEventdata ={
	 "subject":"GTMy Sync", 
	"starttime":"2012-11-17T09:00:00Z", 
	"sMonthYr":"2012-11-17",
	"eMonthYr":"2012-11-17",
	"endtime":"2012-11-17T10:00:00Z", 
	"passwordrequired":"false", 
	"conferencecallinfo":"Hybrid", 
	"timezonekey":"", 
	"meetingtype":"Scheduled"
	};


	function initScripts_extnGTM()
	{ 
		var head = document.getElementsByTagName('head')[0];
	    var script = document.createElement('script');
	    script.type = 'text/javascript';
	    script.src = "dateUtil.js";
	    head.appendChild(script);

	}

    var syncGmailtoMeeting= 
	{
		"_xmlhttp":null,
		"storage": chrome.storage.local,
		"calTab":null,
		"gtmExtnAccessToken":null,
		"meetingId":null,
	    "currentActionCall":"createGTMMeeting",
		"selectedEventMeetingId":''
	};

	 function onRequest_extnGTM(request, sender, callback) {
		switch (request.action) {
			case 'showPageAction': showPageAction_extnGTM(callback,sender); break;	
			case 'createGTMeeting': createGTMeeting_extnGTM(request,callback,sender); break;	
			case 'updateGoogleMeeting': updateGTMeeting_extnGTM(request,callback,sender); break;
			case 'getMeetings_extnGTM': getMeetings_extnGTM(request,callback,sender); break;
		}      
	}

	function showPageAction_extnGTM(callback, sender) { 
        chrome.pageAction.show(sender.tab.id);
		chrome.pageAction.setIcon({path: "icon16.png",tabId: sender.tab.id});
				
    }
	function getMeetings_extnGTM(request,callback,sender)
	{	    
		syncGmailtoMeeting.currentActionCall="getGTMMeeting";
		syncGmailtoMeeting.calTab = sender.tab.id;
		syncGmailtoMeeting.selectedEventMeetingId=request.getGoogleEvent;
		var selectedGoogleEvent=request.selectedGoogleEvent;
		var sMonthYr=selectedGoogleEvent.sMonthYr;
		var eMonthYr=selectedGoogleEvent.eMonthYr;

		syncEventdata.subject = selectedGoogleEvent.eventTitle;

		var split_sMonthYr = selectedGoogleEvent.sMonthYr.split("/");
		sMonthYr = split_sMonthYr[2]+"-"+split_sMonthYr[0]+"-"+split_sMonthYr[1];
		syncEventdata.sMonthYr = sMonthYr;

		var split_eMonthYr = selectedGoogleEvent.eMonthYr.split("/");
		eMonthYr = split_eMonthYr[2]+"-"+split_eMonthYr[0]+"-"+split_eMonthYr[1];
		syncEventdata.eMonthYr = eMonthYr;


		var stimeLength = selectedGoogleEvent.sTime.length; 
		var sTime=	(stimeLength==7) ? (selectedGoogleEvent.sTime.substring(0,5)+" "+selectedGoogleEvent.sTime.substring(5,7)) : ((stimeLength==6) ? (selectedGoogleEvent.sTime.substring(0,4)+" "+selectedGoogleEvent.sTime.substring(4,6)) : selectedGoogleEvent.sTime);
		var sdateString = sMonthYr+" "+sTime;
		sdateString = new Date(sdateString);				
		syncEventdata.starttime =sdateString.format("isoUtcDateTime");

		var eTime=	(selectedGoogleEvent.eTime.length==7) ? (selectedGoogleEvent.eTime.substring(0,5)+" "+selectedGoogleEvent.eTime.substring(5,7)) : ( (selectedGoogleEvent.eTime.length==6) ? (selectedGoogleEvent.eTime.substring(0,4)+" "+selectedGoogleEvent.eTime.substring(4,6)) : selectedGoogleEvent.eTime);
		var edateString = eMonthYr+" "+eTime;
		edateString = new Date(edateString);
		syncEventdata.endtime =edateString.format("isoUtcDateTime"); 
		
        checkAccessToken_extnGTM();
	}
	
	function createGTMeeting_extnGTM(request,callback,sender)
	{ 		
	    syncGmailtoMeeting.currentActionCall="createGTMMeeting";
		syncGmailtoMeeting.calTab = sender.tab.id;
		var selectedGoogleEvent=request.selectedGoogleEvent;
		var sMonthYr=selectedGoogleEvent.sMonthYr;
		var eMonthYr=selectedGoogleEvent.eMonthYr;

		syncEventdata.subject = selectedGoogleEvent.eventTitle;

		var split_sMonthYr = selectedGoogleEvent.sMonthYr.split("/");
		sMonthYr = split_sMonthYr[2]+"-"+split_sMonthYr[0]+"-"+split_sMonthYr[1];
		syncEventdata.sMonthYr = sMonthYr;

		var split_eMonthYr = selectedGoogleEvent.eMonthYr.split("/");
		eMonthYr = split_eMonthYr[2]+"-"+split_eMonthYr[0]+"-"+split_eMonthYr[1];
		syncEventdata.eMonthYr = eMonthYr;


		var stimeLength = selectedGoogleEvent.sTime.length; 
		var sTime=	(stimeLength==7) ? (selectedGoogleEvent.sTime.substring(0,5)+" "+selectedGoogleEvent.sTime.substring(5,7)) : ((stimeLength==6) ? (selectedGoogleEvent.sTime.substring(0,4)+" "+selectedGoogleEvent.sTime.substring(4,6)) : selectedGoogleEvent.sTime);
		var sdateString = sMonthYr+" "+sTime;
		sdateString = new Date(sdateString);				
		syncEventdata.starttime =sdateString.format("isoUtcDateTime");

		var eTime=	(selectedGoogleEvent.eTime.length==7) ? (selectedGoogleEvent.eTime.substring(0,5)+" "+selectedGoogleEvent.eTime.substring(5,7)) : ( (selectedGoogleEvent.eTime.length==6) ? (selectedGoogleEvent.eTime.substring(0,4)+" "+selectedGoogleEvent.eTime.substring(4,6)) : selectedGoogleEvent.eTime);
		var edateString = eMonthYr+" "+eTime;
		edateString = new Date(edateString);
		syncEventdata.endtime =edateString.format("isoUtcDateTime"); 
		checkAccessToken_extnGTM();
	}

	function checkAccessToken_extnGTM()
	{ 
			syncGmailtoMeeting.storage.get('gtmExtnAccessToken', function(items) { 
				if(items.gtmExtnAccessToken) 
				{ 
					syncGmailtoMeeting.gtmExtnAccessToken= items.gtmExtnAccessToken;
					if(syncGmailtoMeeting.currentActionCall=="createGTMMeeting")
						checkCreateMeeting_extnGTM(syncGmailtoMeeting.calTab);
					else if(syncGmailtoMeeting.currentActionCall=="getGTMMeeting")
						checkGetMeetings_extnGTM();

				}
				else 	
				{ 
					var urlAuth =  "https://api.citrixonline.com/oauth/authorize?client_id=b29c2a2d56d9050435ad4534619716b3&redirect_uri=http://www.veenome.com";
					chrome.windows.create({'url':urlAuth,'type':"popup",'height':700},
					function(win)
					{ 
						tabid =win.tabs[0].id; 
                        chrome.tabs.onRemoved.addListener(function(tabid,removeInfo) {  
							chrome.tabs.executeScript(syncGmailtoMeeting.calTab, {code:"if(document.getElementById('LoadingDiv').style.display=='block'){document.getElementById('LoadingDiv').style.display='none';}"});  });
						chrome.tabs.onUpdated.addListener( function(tabid, changeInfo) 
															{ 
															   if(changeInfo.url){
																if ( changeInfo.url.indexOf("code")!=-1 ) 
																	{   
																		if ( changeInfo.url.indexOf('?') > -1 ) 
																			{
																				var code = changeInfo.url.substr(changeInfo.url.indexOf('?code=') + 6); 
																				retrieveToken_extnGTM(code,win,syncGmailtoMeeting.calTab);	 
																			}																	
																	}}
															}); 

					});
				}
			});	  
	}

	function retrieveToken_extnGTM(code,win,tabid)
	{ 
		chrome.windows.remove(win.id);
		chrome.windows.getAll({populate: true} , 
			function(windows)
			{ 
				for(var w in windows)
					{   
						var tabs = windows[w].tabs; 
						for(var t in tabs)
							{ 
								if(tabs[t].id==tabid) 
									chrome.tabs.update(tabid, {active: true}); 
							}
					}
			});
		var url = "https://api.citrixonline.com/oauth/access_token?grant_type=authorization_code&code="+code+"&client_id=b29c2a2d56d9050435ad4534619716b3";
		var xmlhttp=  new XMLHttpRequest();
		xmlhttp.open("GET",url,false);
		xmlhttp.send();
		if (xmlhttp.status == 200)
		{ 
			var data=JSON.parse(xmlhttp.responseText); 
			var access_token=data.access_token;
			syncGmailtoMeeting.gtmExtnAccessToken=access_token;
			var refresh_token=data.refresh_token; 
			syncGmailtoMeeting.storage.set({'gtmExtnAccessToken': access_token}, 
				function()
				    {
				          if(syncGmailtoMeeting.currentActionCall=="createGTMMeeting")
						checkCreateMeeting_extnGTM(tabid);
					else if(syncGmailtoMeeting.currentActionCall=="getGTMMeeting")
						checkGetMeetings_extnGTM();
			             // checkCreateMeeting_extnGTM(tabid);
			        });
	    }
		
    }
   
    function checkGetMeetings_extnGTM()
	{
       var url = "https://api.citrixonline.com/G2M/rest/meetings/"+syncGmailtoMeeting.selectedEventMeetingId;
		var authoHeader = "OAuth oauth_token="+syncGmailtoMeeting.gtmExtnAccessToken ;  
		var xmlhttp=  new XMLHttpRequest();
		xmlhttp.open("GET",url,false);
		xmlhttp.setRequestHeader("Accept", "application/json");
			xmlhttp.setRequestHeader("Content-type", "application/json");
			xmlhttp.setRequestHeader("Authorization",authoHeader);
		xmlhttp.send();
		if (xmlhttp.status == 200)
		{ 
			var data=JSON.parse(xmlhttp.responseText); 

			syncGmailtoMeeting.storage.get('gtmExtnAccessToken', function(items) { 
		if(items.gtmExtnAccessToken) 
		{ 
			syncGmailtoMeeting.gtmExtnAccessToken=items.gtmExtnAccessToken; 
			var updateGTMUrl= "https://api.citrixonline.com/G2M/rest/meetings/"+syncGmailtoMeeting.selectedEventMeetingId;
			var xmlhttp= new XMLHttpRequest();
			xmlhttp.open("PUT",updateGTMUrl,false);
			xmlhttp.setRequestHeader("Accept", "application/json");
			xmlhttp.setRequestHeader("Content-type", "application/json");
			xmlhttp.setRequestHeader("Authorization",authoHeader );

			var syncEventdataPost =JSON.stringify(syncEventdata);  	 
			xmlhttp.send(syncEventdataPost); 
			if(xmlhttp.status == 204)
			{  
				chrome.tabs.sendMessage(syncGmailtoMeeting.calTab, {update_descText: syncEventdata.starttime,action:'updateDesc'},function(response){} );

				return;
			}
			else
				alert("Update Failed.");
		}
		});
              
							
	    }
		if(xmlhttp.status == 404)
		{
			alert("No corresponding meeting found to update. Please add the meeting.");
		}

	}


	function checkCreateMeeting_extnGTM(tabid)
	{
		syncGmailtoMeeting.storage.get('gtmExtnAccessToken', function(items) { 
		if(items.gtmExtnAccessToken) 
		{ 
			syncGmailtoMeeting.gtmExtnAccessToken=items.gtmExtnAccessToken; 
			url = "https://api.citrixonline.com/G2M/rest/meetings";
			var authoHeader = "OAuth oauth_token="+syncGmailtoMeeting.gtmExtnAccessToken;  
			var xmlhttp= new XMLHttpRequest();
			xmlhttp.open("POST",url,false);
			xmlhttp.setRequestHeader("Accept", "application/json");
			xmlhttp.setRequestHeader("Content-type", "application/json");
			xmlhttp.setRequestHeader("Authorization",authoHeader );

			var syncEventdataPost =JSON.stringify(syncEventdata);  	 
			xmlhttp.send(syncEventdataPost); 
			if(xmlhttp.status != 201)
			{  
				syncGmailtoMeeting.storage.remove('gtmExtnAccessToken');
				checkAccessToken_extnGTM();
				return;
			}
			if (xmlhttp.status == 201)
			{ 
				result = JSON.parse(xmlhttp.responseText); 
				syncGmailtoMeeting.meetingId = result[0].meetingid; 

				alert("Selected Meeting Saved in GTM.");

				var getMeetingUrl = "https://api.citrixonline.com/G2M/rest/meetings/"+syncGmailtoMeeting.meetingId;
				var xmlhttp1= new XMLHttpRequest();
				xmlhttp1.open("GET",getMeetingUrl,false);

				xmlhttp1.setRequestHeader("Accept", "application/json");

				xmlhttp1.setRequestHeader("Content-type", "application/json");
				xmlhttp1.setRequestHeader("Authorization",authoHeader );
				xmlhttp1.send(); 

				var resultDesc =xmlhttp1.responseText; 
				resultDesc = JSON.parse(xmlhttp1.responseText);
				chrome.tabs.sendMessage(syncGmailtoMeeting.calTab, {result_desc: resultDesc,action:'populateDesc'},function(response){} );

			}
		}
		});
	}


	function getXMLHttp()
	{
			var xx;
			if (syncGmailtoMeeting._xmlhttp)
			{
				xx = syncGmailtoMeeting._xmlhttp;
				xx.abort();
			}
			else
			{
				if (window.XMLHttpRequest) xx = new XMLHttpRequest();
				else xx = new ActiveXObject("Microsoft.XMLHTTP");
				syncGmailtoMeeting._xmlhttp = xx;
			}
			return xx;
	  }

 

chrome.extension.onRequest.addListener(onRequest_extnGTM);
