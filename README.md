# Description

Adds some basic enhancements to the [Veenome Calendar Integration](https://chrome.google.com/webstore/detail/veenome-calendar-integrat/pgdcbdeihbhnmdmdmlfekaiddkinplhe).  

## Enhancements 

- Trim out extra fat from meeting invite
- Space out meeting invite if there is already a description
- Set the location to GoTo Meeting

# Installation

1. Clone down the Git Repo
1. Open Chrome->Tools->Extensions
1. Click the Developer Mode Checkbox in the top right
1. Click Load Unpacked Extension
1. Select the top level folder
1. Turn off other trello extension

# Get Updates

1. Watch this repo for changes
1. In the top level folder run `git pull origin master`
1. Open Chrome->Tools->Extensions
1. Click the "Reload" link next to Veenome Calendar Integration